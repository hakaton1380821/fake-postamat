import './App.css';
import {useEffect} from "react";
import * as widget  from '@dmitry_zolotukhin/feedback-widget'

function App() {
  useEffect(()=>{
    console.log('PRE WIDGET')
  }, [])

  return (
    <div className="App">
      <button onClick={() => widget.widget(123)} className="postamat-submit">Посылка получена</button>
      <div id="div1"></div>
    </div>
  );
}

export default App;
